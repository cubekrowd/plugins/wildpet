/*
 * Copyright (C) 2018 Kapurai (Lekro)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.cubekrowd.wildpet;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class WildPetPlugin extends JavaPlugin implements Listener {
    
    private Set<String> mobs;
    
    @Override
    public void onEnable() {
        // Initialize configuration
        saveDefaultConfig();
        
        Material eggItem = getMaterial();
        
        mobs = getConfig().getConfigurationSection("lore").getKeys(false);
        mobs.forEach(m -> {
            registerRecipe(eggItem, m);
        });
        getServer().getPluginManager().registerEvents(this, this);
        
    }
    
    private Material getMaterial() {
        return Material.matchMaterial(getConfig().getString("egg-item"));
    }
    
    private String getLore(String mob) {
        return getConfig().getString("lore-prefix") + getConfig().getConfigurationSection("lore").get(mob);
    }
    
    private void registerRecipe(Material mat, String mob) {
        
        ItemStack item = new ItemStack(mat);
        ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList(new String[] {getLore(mob)}));
        item.setItemMeta(meta);
        
        List<?> recipes = getConfig().getConfigurationSection("recipes").getList(mob);
        
        int numRecipes = 0;
        for (int i = 0; i < recipes.size(); i++) {
            if (!(recipes.get(i) instanceof List)) {
                getLogger().warning("Recipe " + i + " for " + mob + " is invalid! Skipping.");
                continue;
            }
            
            List<?> r = (List<?>) recipes.get(i);
            ShapelessRecipe recipe = new ShapelessRecipe(new NamespacedKey(this, mob+"_"+i), item);
            boolean valid = true;
            for (int j = 0; j < r.size(); j++) {
                String mStr = r.get(j).toString();
                Material m = Material.matchMaterial(mStr);
                if (m == null) {
                    getLogger().warning("In recipe " + i + " for " + mob + ", found invalid item " + mStr);
                    valid = false;
                    break;
                }
                recipe.addIngredient(m);
            }
            
            if (valid) {
                getServer().addRecipe(recipe);
                numRecipes++;
            }
        }
        
        getLogger().info("Registered " + numRecipes + " recipes for " + mob + ".");
        
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent ev) {
        
        if (ev.getAction() != Action.RIGHT_CLICK_AIR && ev.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        
        Player p = ev.getPlayer();
        ItemStack item = ev.getItem();
        if (item == null) return;
        
        if (item.getType() == getMaterial()) {
            if (item.getItemMeta() == null) return;
            List<String> lore = item.getItemMeta().getLore();
            if (lore == null) return;
            if (lore.size() != 1) return;
            for (String mob : mobs) {
                if (lore.get(0).equals(getLore(mob))) {
                    p.sendMessage(ChatColor.GREEN + "Spawned a " + mob + "!");
                    item.setAmount(item.getAmount() - 1);
                    p.getWorld().spawnEntity(p.getLocation(), EntityType.fromName(mob));
                    break;
                }
            }
        }
    }
    
    @Override
    public void onDisable() {
        
    }

}
