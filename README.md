# CubeKrowd WildPet

Add recipes to craft special items that spawn mobs.
Items are one-time use only and use lore to match the
item to the mob to spawn.

All recipes are shapeless.

## Default recipes

- **Ocelot**: any horse armor with a raw fish
- **Wolf**: any horse armor with a bone

## Configuration

The recipes and mobs possible can be modified
in `config.yml`. Every defined mob must have a lore
defined in the `lore` section. An arbitrary string can
be used, but since the user will see it, generally a
human-readable string is best.

`lore-prefix` is always prepended to all lore. Color
codes are supported using the section sign as in the
example.

`egg-item` defines the item to grant users in order to
spawn the mob.

## License

```
Copyright (C) 2018 Kapurai (Lekro)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
